<?php

/**
 * @file
 * Contains cmm.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function cmm_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the cmm module.
    case 'help.page.cmm':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides workflow for currently logged in users during deployment process.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function cmm_form_system_site_maintenance_mode_alter(&$form, FormStateInterface $form_state, $form_id) {

  $form['logout'] = [
    '#type' => 'details',
    '#title' => t('Extra settings'),
  ];

  $config = \Drupal::config('cmm.settings');
  $complete_base_url = \Drupal::service('router.request_context')->getCompleteBaseUrl();

  $form['logout']['description'] = [
    '#type' => 'markup',
    '#markup' => t("If you need to perform maintenance on your website while users are still logged in, you can define a message to notify them of the upcoming maintenance. To do this, save the form without selecting the 'Log out all users who are currently logged in' checkbox. When you're ready to switch the site to maintenance mode, select this option. All users, except the admin, will be logged out and the site will be in maintenance mode. The `/user` (user.login) route will also be unavailable, so users won't be able to log in. If you need to log in during maintenance mode, you can disable it using a hidden login page. To set up a hidden login page, provide a hash in the 'Hash for hidden login page' field, or use the default hash. Once set up, you can access the hidden login page during maintenance mode by visiting the hidden path @site/hidden/login/{hash}", ['@site' => $complete_base_url]),
  ];

  $form['logout']['logout_message'] = [
    '#type' => 'textarea',
    '#title' => t('Upcoming maintenance mode message'),
    '#description' => t('You can display a message to users who are logged into a website, asking them to save their work before the site goes into maintenance mode and their sessions are cleared. For example: "Please save your work and log out of the system. At 09:00 AM EST, we will switch to maintenance mode, and all logged-in users will be automatically logged out."'),
    '#default_value' => $config->get('logout_message'),
  ];


  $form['logout']['login_hash'] = [
    '#type' => 'textfield',
    '#title' => t('Hash for hidden login page'),
    '#required' => TRUE,
    '#description' => t('To access the hidden login page during maintenance mode, please provide the corresponding hash code by visiting @site/hidden/login/{hash}', ['@site' => $complete_base_url]),
    '#default_value' => $config->get('hash'),
  ];

  $form['#submit'][] = 'cmm_logout_users';
}

/**
 * Submit function().
 */
function cmm_logout_users($form, FormStateInterface $form_state) {
  $values = $form_state->getValues();

  $config = \Drupal::service('config.factory')->getEditable('cmm.settings');
  $config->set('logout_message', $values['logout_message'])->save();
  $config->set('hash', $values['login_hash'])->save();

  // If there's a logout checkbox checked.
  if (!empty($values['logout_users'])) {
    // Get current sessions except the one for admin.
    $query = \Drupal::database()->select('sessions', 's');
    $query->leftJoin('users_field_data', 'u', 'u.uid = s.uid');
    $query->condition('s.uid', 1, '<>');
    $query->fields('s', ['uid']);
    $query->fields('u', ['name']);
    $result = $query->execute()->fetchAll();
    // Destroy sessions.
    if (!empty($result)) {
      foreach ($result as $session) {
        $session_manager = \Drupal::service('session_manager');
        $session_manager->delete($session->uid);
      }
      \Drupal::messenger()->addMessage(t('@number of users has been logged out.', ['@number' => count($result)]));
    }
  }
}

/**
 * Implements hook_preprocess_page().
 */
function cmm_preprocess_page(&$variables) {
  $config = \Drupal::config('cmm.settings');
  if (!empty($config->get('logout_message')) && \Drupal::currentUser()->isAuthenticated()) {
    \Drupal::messenger()->addWarning($config->get('logout_message'));
  }
}
