<?php

namespace Drupal\cmm\Form;

use Drupal\Core\Url;
use Drupal\user\Form\UserLoginForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a user login hidden form.
 */
class UserLoginHiddenForm extends UserLoginForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hash = '') {
    $form = parent::buildForm($form, $form_state);
    $maintenance_mode = \Drupal::state()->get('system.maintenance_mode');
    $config = \Drupal::config('cmm.settings');
    if (!empty($config->get('hash')) && $hash == $config->get('hash') && $maintenance_mode == '1') {
      return $form;
    }
    else {}
  }
}