# Custom Maintenance Mode

This module enhances the default maintenance mode feature by providing the ability to log out all currently logged in users and leave them a message about the upcoming maintenance. It also disables access to the /user route during maintenance mode to prevent regular users from accessing the login form. However, it enables a hidden path for admins to easily log in.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/cmm).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/cmm).

## Requirements

* This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers:

- [Rajesh Bhimani](https://www.drupal.org/u/rajesh-bhimani)

Supporting organizations:

* [Skynet Technologies USA LLC](https://www.skynettechnologies.com)
